import React from 'react';
import {Link} from 'react-router-dom';

import styles from './About.module.css';

const About: React.FC = () => {
  return (
    <div className={styles.wrap}>
      <p>
        <Link to={'/tickers'}>Go to tickers page</Link>
      </p>
    </div>
  );
}

export default About;
