import { BrowserRouter, Route, Redirect, Switch } from 'react-router-dom';

import TickersList from '@routes/TicketsList/TickersList';
import About from '@routes/About/About';
import Tabs from '@components/Tabs/Tabs';
import TabLink from '@components/Tabs/TabLink';

function App() {
  return (
    <BrowserRouter>
      <Tabs>
        <TabLink to={'about'}>About</TabLink>
        <TabLink to={'tickers'}>Tickers</TabLink>
      </Tabs>
      <Switch>
        <Route path="/tickers" component={TickersList} />
        <Route path="/about" component={About} />
        <Redirect from="/" to="/about" />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
