import React from 'react';
import {NavLink} from 'react-router-dom';

import styles from './Tabs.module.css';

interface IProps {
  to: string
}

const TabLink: React.FC<IProps> = ({to, children}) => {
  return <NavLink className={styles.tab}
                  activeClassName={styles.active}
                  to={to}>{children}</NavLink>;
}

export default TabLink;
