import React, {useEffect, useRef} from 'react';

import styles from './ObservableField.module.css';
import classNames from 'classnames';

interface IProps {
  val: string | number
}

const usePrevious = (val: string|number) => {
  const prevVal = useRef(val);
  useEffect(() => {
    prevVal.current = val;
  }, [val])
  return prevVal.current;
}

const ObservableField: React.FC<IProps> = ({val, children}) => {
  const prevVal = usePrevious(val);
  return <span className={classNames({
    [styles.text]: true,
    [styles.more]: prevVal > val,
    [styles.less]: prevVal < val,
  })}>{val}</span>;
}

export default ObservableField;
