const {override, addDecoratorsLegacy, addPostcssPlugins, addWebpackAlias} = require('customize-cra');
const path = require("path");

module.exports = override(
  addDecoratorsLegacy(),
  addPostcssPlugins([
    require('postcss-nested'),
    require('autoprefixer')]
  ),
  addWebpackAlias({
    //["ag-grid-react$"]: path.resolve(__dirname, "src/shared/agGridWrapper.js")
    '@': path.resolve(__dirname, './src/'),
    '@components': path.resolve(__dirname, './src/components'),
    '@routes': path.resolve(__dirname, './src/routes'),
    '@types': path.resolve(__dirname, './src/types'),
    '@store': path.resolve(__dirname, './src/store'),
    //"@components/*": "components/*",
  })
);
